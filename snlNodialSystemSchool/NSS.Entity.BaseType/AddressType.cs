﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Entity.BaseType
{
    public static partial class AddressType
    {
        internal const string Active = "A";
        internal const string Inactive = "I";
    }
}
