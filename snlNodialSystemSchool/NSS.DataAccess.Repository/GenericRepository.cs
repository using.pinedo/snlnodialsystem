﻿using Dapper;
using Dapper.Contrib;
using Dapper.Contrib.Extensions;
using NSS.Interface.Repository;
using NSS.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NSS.DataAccess.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly string _connectionString;

        public GenericRepository(string connectionString){
            SqlMapperExtensions.TableNameMapper = (type) => { return type.Name; };
            this._connectionString = connectionString;
        }

        public IEnumerable<TEntity> GetAll(){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<TEntity>();
            }
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate){
            //using (var connection = new SqlConnection(_connectionString))
            //{
            //    //return connection.GetAll<TEntity>().Where(predicate);
            //    return connection.GetAll<TEntity>().Where<TEntity>(predicate);
            //}

            throw new NotImplementedException();
        }

        public TEntity GetById(int id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.Get<TEntity>(id);
            }
        }

        public int add(TEntity entity){
            using (var connection = new SqlConnection(_connectionString)){
                return (int)connection.Insert(entity);
            }
        }

        public bool Update(TEntity entity){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.Update(entity);
            }
        }

        public bool Remove(TEntity entity){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.Delete(entity);
            }
        }

    }
}
