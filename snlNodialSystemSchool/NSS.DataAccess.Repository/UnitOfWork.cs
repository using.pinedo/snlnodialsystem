﻿using NSS.Interface.Repository;
using System;
using NSS.Interface.Repository.INodialBase;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSS.DataAccess.Repository.NodialBase;

namespace NSS.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(string connectionString)
        {
            ClassRoom = new ClassRoomRepository(connectionString);
            Educator = new EducatorRepository(connectionString);

            Learner = new LearnerRepository(connectionString);

            School = new SchoolRepository(connectionString);

            Substitute = new SubstituteRepository(connectionString);
        }
        public IClassRoomRepository ClassRoom
        {
            get;
            private set;
        }

        public IEducatorRepository Educator
        {
            get;
            private set;
        }

        public IEducaterAddressRepository EducaterAddress
        {
            get;
            private set;
        }

        public IEducaterExploitRepository EducaterExploit
        {
            get;
            private set;
        }

        public IEducaterRecordsRepository EducaterRecords
        {
            get;
            private set;
        }

        public IEmployeeRepository Employee
        {
            get;
            private set;
        }

        public IGradeRepository Grade
        {
            get;
            private set;
        }

        public ILearnerRepository Learner
        {
            get;
            private set;
        }

        public ILearnerRecordsRepository LearnerRecords
        {
            get;
            private set;
        }

        public ISchoolRepository School
        {
            get;
            private set;
        }

        public ISeasonRepository Season
        {
            get;
            private set;
        }

        public ISemesterRepository Semester
        {
            get;
            private set;
        }

        public ISubGradeRepository SubGrade
        {
            get;
            private set;
        }

        public ISubjectRepository Subject
        {
            get;
            private set;
        }

        public ISubstituteRepository Substitute
        {
            get;
            private set;
        }

        public ISubstituteAddressRepository SubstituteAddress
        {
            get;
            private set;
        }

        public ITurnRepository Turn
        {
            get;
            private set;
        }

        public int Complete()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
