﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class SubstituteRepository : GenericRepository<Substitute>, ISubstituteRepository
    {
        public SubstituteRepository(string connectionString)
            : base(connectionString){

        }

        public new Substitute GetById(int id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Substitute>().Where(substitute => substitute.SubstituteNo.Equals(id)).First();
            }
        }

        public new bool Update(Substitute substitute)
        {
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        StatusCode = substitute.StatusCode,

                        Gender = substitute.Gender,
                        Title = substitute.Title,
                        FirstName = substitute.FirstName,
                        LastName = substitute.LastName,
                        MiddleName = substitute.MiddleName,
                        MaidenName = substitute.MaidenName,
                        //FullName = substitute.FullName,
                        SubstituteEmail = substitute.SubstituteEmail,
                        LicenseNumber = substitute.LicenseNumber,
                        Phone1 = substitute.Phone1,
                        Phone2 = substitute.Phone2,
                        Info1 = substitute.Info1,
                        Info2 = substitute.Info2,
                        Info3 = substitute.Info3,
                        Notes = substitute.Notes,
                        ExternalId = substitute.ExternalId,
                        Blocked = substitute.Blocked,
                        CreationDate = substitute.CreationDate,
                        CreatedBy = substitute.CreatedBy,
                        //ChangeDate = substitute.ChangeDate,
                        //ModifiedBy = substitute.ModifiedBy,

                        myId = substitute.SubstituteNo
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Remove(int id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE SUBSTITUTE SET "
                + "StatusCode=@StatusCode,"
                + "Gender=@Gender,"
                + "Title=@Title,"
                + "FirstName=@FirstName,"
                + "LastName=@LastName,"
                + "MiddleName=@MiddleName,"
                + "MaidenName=@MaidenName,"

                //+ "FullName=RTRIM(LTRIM(RTRIM(LTRIM(@FirstName + ' ' + @LastName)) + ' ' + ISNULL(@MiddleName,''))),"

                + "SubstituteEmail=@SubstituteEmail,"
                + "LicenseNumber=@LicenseNumber,"
                + "Phone1=@Phone1,"
                + "Phone2=@Phone2,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN' "

                + "WHERE SubstituteNo=@myId";

        private string qryDelete = "DELETE SUBSTITUTE WHERE SubstituteNo=@myId";
    }
}
