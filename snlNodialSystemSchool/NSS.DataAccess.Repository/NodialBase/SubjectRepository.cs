﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class SubjectRepository : GenericRepository<Subject>, ISubjectRepository
    {
        public SubjectRepository(string connectionString)
            : base(connectionString){

        }

        public Subject GetById(string id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Subject>().Where(subject => subject.SubjectId.Equals(id)).First();
            }
        }

        public new bool Update(Subject subject){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        SubjectName = subject.SubjectName,
                        SubjectCode = subject.SubjectCode,
                        Info1 = subject.Info1,
                        Info2 = subject.Info2,
                        Info3 = subject.Info3,
                        Notes = subject.Notes,
                        ExternalId = subject.ExternalId,
                        Blocked = subject.Blocked,
                        CreationDate = subject.CreationDate,
                        CreatedBy = subject.CreatedBy,
                        //ChangeDate = subject.ChangeDate,
                        //ModifiedBy = subject.ModifiedBy,

                        myId = subject.SubjectId
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE SUBJECT SET "
                + "SubjectName=@SubjectName,"
                + "SubjectCode=@SubjectCode,"

                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE SubjectId=@myId";

        private string qryDelete = "DELETE SUBJECT WHERE SubjectId=@myId";
    }
}
