﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class SemesterRepository : GenericRepository<Semester>, ISemesterRepository
    {
        public SemesterRepository(string connectionString)
            : base(connectionString){

        }

        public Semester GetById(string id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Semester>().Where(semester => semester.SemesterCode.Equals(id)).First();
            }
        }

        public new bool Update(Semester semester){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        SeasonCode = semester.SeasonCode,

                        SemesterName = semester.SemesterName,
                        Info1 = semester.Info1,
                        Info2 = semester.Info2,
                        Info3 = semester.Info3,
                        Notes = semester.Notes,
                        ExternalId = semester.ExternalId,
                        Blocked = semester.Blocked,
                        CreationDate = semester.CreationDate,
                        CreatedBy = semester.CreatedBy,
                        //ChangeDate = semester.ChangeDate,
                        //ModifiedBy = semester.ModifiedBy,

                        myId = semester.SemesterCode
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE SEMESTER SET "
                + "SeasonCode=@SeasonCode,"
                + "SemesterName=@SemesterName,"

                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE SemesterCode=@myId";

        private string qryDelete = "DELETE SEMESTER WHERE SemesterCode=@myId";
    }
}
