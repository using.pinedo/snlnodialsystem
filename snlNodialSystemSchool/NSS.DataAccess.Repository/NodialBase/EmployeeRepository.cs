﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(string connectionString)
            : base(connectionString){

        }

        public Employee GetById(string id)
        {
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Employee>().Where(employee => employee.EmployeeCode.Equals(id)).First();
            }
        }

        public new bool Update(Employee employee)
        {
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        StatusCode = employee.StatusCode,

                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        MiddleName = employee.MiddleName,
                        //Password = employee.Password,
                        //FullName = employee.FullName,

                        Address1 = employee.Address1,
                        Address2 = employee.Address2,
                        City = employee.City,
                        State = employee.State,
                        ZipCode = employee.ZipCode,

                        Email = employee.Email,

                        Phone1 = employee.Phone1,
                        Phone2 = employee.Phone2,
                        Info1 = employee.Info1,
                        Info2 = employee.Info2,
                        Info3 = employee.Info3,
                        Notes = employee.Notes,
                        Gender = employee.Gender,
                        ExternalId = employee.ExternalId,
                        Deleted = employee.Deleted,
                        CreationDate = employee.CreationDate,
                        CreatedBy = employee.CreatedBy,
                        //ChangeDate = employee.ChangeDate,
                        //ModifiedBy = employee.ModifiedBy,

                        myId = employee.EmployeeCode
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE EMPLOYEE SET "
                + "StatusCode=@StatusCode,"

                + "FirstName=@FirstName,"
                + "LastName=@LastName,"
                + "MiddleName=@MiddleName,"
                //+ "Password=@Password,"

                + "FullName=RTRIM(LTRIM(RTRIM(LTRIM(@FirstName + ' ' + @LastName)) + ' ' + ISNULL(@MiddleName,''))),"

                + "Address1=@Address1,"
                + "Address2=@Address2,"
                + "City=@City,"
                + "State=@State,"
                + "ZipCode=@ZipCode,"

                + "Email=@Email,"
                + "Phone1=@Phone1,"
                + "Phone2=@Phone2,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "Gender=@Gender,"
                + "ExternalId=@ExternalId,"
                + "Deleted=@Deleted,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE EmployeeCode=@myId";

        private string qryDelete = "DELETE EMPLOYEE WHERE EmployeeCode=@myId";
    }
}
