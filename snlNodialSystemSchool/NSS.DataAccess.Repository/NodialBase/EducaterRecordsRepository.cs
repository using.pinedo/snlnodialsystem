﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class EducaterRecordsRepository : GenericRepository<EducaterRecords>, IEducaterRecordsRepository
    {
        public EducaterRecordsRepository(string connectionString)
            : base(connectionString){

        }
    }
}
