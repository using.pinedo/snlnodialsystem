﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class SubstituteAddressRepository : GenericRepository<SubstituteAddress>, ISubstituteAddressRepository
    {
        public SubstituteAddressRepository(string connectionString)
            : base(connectionString){

        }

        public SubstituteAddress GetById(string id, int no){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<SubstituteAddress>().Where(substituteAddress => substituteAddress.SubstituteNo.Equals(id) && substituteAddress.AddressNo.Equals(no)).First();
            }
        }

        public new bool Update(SubstituteAddress substituteAddress)
        {
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        StatusCode = substituteAddress.StatusCode,

                        Label = substituteAddress.Label,
                        Address1 = substituteAddress.Address1,
                        Address2 = substituteAddress.Address2,
                        City = substituteAddress.City,
                        State = substituteAddress.State,
                        ZipCode = substituteAddress.ZipCode,
                        CountryName = substituteAddress.CountryName,
                        Ubigeo = substituteAddress.Ubigeo,
                        Phone1 = substituteAddress.Phone1,
                        Phone2 = substituteAddress.Phone2,
                        Info1 = substituteAddress.Info1,
                        Info2 = substituteAddress.Info2,
                        Info3 = substituteAddress.Info3,
                        Notes = substituteAddress.Notes,
                        IsMainAddress = substituteAddress.IsMainAddress,
                        CreationDate = substituteAddress.CreationDate,
                        CreatedBy = substituteAddress.CreatedBy,
                        //ChangeDate = substituteAddress.ChangeDate,
                        //ModifiedBy = substituteAddress.ModifiedBy,

                        myId = substituteAddress.SubstituteNo,
                        myAddressNo = substituteAddress.AddressNo
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id, int no){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, 
                    new { myId = id,
                        myAddressNo=no
                    });
                return Convert.ToBoolean(result);
            }
        }


        private string qryUpdate = "UPDATE SUBSTITUTE_ADDRESS SET "
                + "StatusCode=@StatusCode,"
                + "Label=@Label,"
                + "Address1=@Address1,"
                + "Address2=@Address2,"
                + "City=@City,"
                + "State=@State,"
                + "ZipCode=@ZipCode,"
                + "CountryName=@CountryName,"
                + "Ubigeo=@Ubigeo,"
                + "Phone1=@Phone1,"
                + "Phone2=@Phone2,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "IsMainAddress=@IsMainAddress,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE SubstituteNo=@myId "
                + "AND AddressNo=@myAddressNo";

        private string qryDelete = "DELETE SUBSTITUTE_ADDRESS WHERE SubstituteNo=@myId AND AddressNo=@myAddressNo";
    }
}
