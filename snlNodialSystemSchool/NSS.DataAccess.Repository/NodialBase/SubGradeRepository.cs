﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class SubGradeRepository : GenericRepository<SubGrade>, ISubGradeRepository
    {
        public SubGradeRepository(string connectionString)
            : base(connectionString){

        }

        public SubGrade GetById(string id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<SubGrade>().Where(subGrade => subGrade.SubGradeCode.Equals(id)).First();
            }
        }

        public new bool Update(SubGrade subGrade)
        {
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        GradeCode = subGrade.GradeCode,
                        SeasonCode = subGrade.SeasonCode,

                        SemesterName = subGrade.SubGradeName,
                        Info1 = subGrade.Info1,
                        Info2 = subGrade.Info2,
                        Info3 = subGrade.Info3,
                        Notes = subGrade.Notes,
                        ExternalId = subGrade.ExternalId,
                        Blocked = subGrade.Blocked,
                        CreationDate = subGrade.CreationDate,
                        CreatedBy = subGrade.CreatedBy,
                        //ChangeDate = subGrade.ChangeDate,
                        //ModifiedBy = subGrade.ModifiedBy,

                        myId = subGrade.SubGradeCode
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE SUBGRADE SET "
                + "GradeCode=@GradeCode,"
                + "SeasonCode=@SeasonCode,"
                + "SubGradeName=@SubGradeName,"

                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE SubGradeCode=@myId";

        private string qryDelete = "DELETE SUBGRADE WHERE SubGradeCode=@myId";
    }
}
