﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class EducatorRepository : GenericRepository<Educator>, IEducatorRepository
    {
        public EducatorRepository(string connectionString)
            : base(connectionString){

        }

        public new Educator GetById(int id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Educator>().Where(educator => educator.EducatorNo.Equals(id)).First();
            }
        }

        public new bool Update(Educator educator){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        StatusCode = educator.StatusCode,

                        Gender = educator.Gender,
                        Title = educator.Title,
                        Grade = educator.Grade,
                        FirstName = educator.FirstName,
                        LastName = educator.LastName,
                        MiddleName = educator.MiddleName,
                        MaidenName = educator.MaidenName,
                        //FullName = educator.FullName,
                        EducatorEmail = educator.EducatorEmail,
                        LicenseNumber = educator.LicenseNumber,
                        Phone1 = educator.Phone1,
                        Phone2 = educator.Phone2,
                        Info1 = educator.Info1,
                        Info2 = educator.Info2,
                        Info3 = educator.Info3,
                        Notes = educator.Notes,
                        ExternalId = educator.ExternalId,
                        Blocked = educator.Blocked,
                        CreationDate = educator.CreationDate,
                        CreatedBy = educator.CreatedBy,
                        //ChangeDate = educator.ChangeDate,
                        //ModifiedBy = educator.ModifiedBy,

                        myId = educator.EducatorNo
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Remove(int id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE EDUCATOR SET "
                + "StatusCode=@StatusCode,"
                + "Gender=@Gender,"
                + "Title=@Title,"
                + "Grade=@Grade,"
                + "FirstName=@FirstName,"
                + "LastName=@LastName,"
                + "MiddleName=@MiddleName,"
                + "MaidenName=@MaidenName,"

                + "FullName=RTRIM(LTRIM(RTRIM(LTRIM(@FirstName + ' ' + @LastName)) + ' ' + ISNULL(@MiddleName,''))),"

                + "EducatorEmail=@EducatorEmail,"
                + "LicenseNumber=@LicenseNumber,"
                + "Phone1=@Phone1,"
                + "Phone2=@Phone2,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN' "

                + "WHERE EducatorNo=@myId";

        private string qryDelete = "DELETE EDUCATOR WHERE EducatorNo=@myId";

    }
}
