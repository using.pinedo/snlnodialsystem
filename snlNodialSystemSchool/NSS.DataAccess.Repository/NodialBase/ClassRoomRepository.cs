﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class ClassRoomRepository : GenericRepository<ClassRoom>, IClassRoomRepository
    {
        public ClassRoomRepository(string connectionString)
            : base(connectionString){

        }

        public ClassRoom GetById(string id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<ClassRoom>().Where(classRoom => classRoom.ClassRoomCode.Equals(id)).First();
            }
        }

        public new bool Update(ClassRoom classRoom){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        TurnCode = classRoom.TurnCode,
                        SubGradeCode = classRoom.SubGradeCode,
                        GradeCode = classRoom.GradeCode,
                        SeasonCode = classRoom.SeasonCode,

                        ClassRoomName = classRoom.ClassRoomName,
                        Info1 = classRoom.Info1,
                        Info2 = classRoom.Info2,
                        Info3 = classRoom.Info3,
                        Notes = classRoom.Notes,
                        ExternalId = classRoom.ExternalId,
                        Blocked = classRoom.Blocked,
                        CreationDate = classRoom.CreationDate,
                        CreatedBy = classRoom.CreatedBy,
                        ChangeDate = classRoom.ChangeDate,
                        ModifiedBy = classRoom.ModifiedBy,

                        myId = classRoom.ClassRoomCode
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }


        private string qryUpdate = "UPDATE CLASSROOM SET "
                + "TurnCode=@TurnCode,"
                + "SubGradeCode=@SubGradeCode,"
                + "GradeCode=@GradeCode,"
                + "SeasonCode=@SeasonCode,"
                + "ClassRoomName=@ClassRoomName,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE ClassRoomCode=@myId";

        private string qryDelete = "DELETE CLASSROOM WHERE ClassRoomCode=@myId";

    }
}
