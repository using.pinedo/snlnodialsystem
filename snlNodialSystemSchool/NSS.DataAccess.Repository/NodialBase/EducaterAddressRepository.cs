﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class EducaterAddressRepository : GenericRepository<EducaterAddress>, IEducaterAddressRepository
    {
        public EducaterAddressRepository(string connectionString)
            : base(connectionString){

        }

        public EducaterAddress GetById(string id,int no){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<EducaterAddress>().Where(educaterAddress => educaterAddress.EducaterNo.Equals(id) && educaterAddress.AddressNo.Equals(no)).First();
            }
        }

        public new bool Update(EducaterAddress educaterAddress){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        StatusCode = educaterAddress.StatusCode,

                        Label = educaterAddress.Label,
                        Address1 = educaterAddress.Address1,
                        Address2 = educaterAddress.Address2,
                        City = educaterAddress.City,
                        State = educaterAddress.State,
                        ZipCode = educaterAddress.ZipCode,
                        CountryName = educaterAddress.CountryName,
                        Phone = educaterAddress.Phone,
                        Recipient = educaterAddress.Recipient,
                        IsMainAddress = educaterAddress.IsMainAddress,
                        Info1 = educaterAddress.Info1,
                        Info2 = educaterAddress.Info2,
                        Info3 = educaterAddress.Info3,
                        Notes = educaterAddress.Notes,
                        CreationDate = educaterAddress.CreationDate,
                        CreatedBy = educaterAddress.CreatedBy,
                        //ChangeDate = educaterAddress.ChangeDate,
                        //ModifiedBy = educaterAddress.ModifiedBy,

                        myId = educaterAddress.EducaterNo,
                        myAddressNo=educaterAddress.AddressNo
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id, int no){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, 
                    new { myId = id,
                        myAddressNo=no
                    });
                return Convert.ToBoolean(result);
            }
        }


        private string qryUpdate = "UPDATE EDUCATER_ADDRESS SET "
                + "StatusCode=@StatusCode,"
                + "Label=@Label,"
                + "Address1=@Address1,"
                + "Address2=@Address2,"
                + "City=@City,"
                + "State=@State,"
                + "ZipCode=@ZipCode,"
                + "CountryName=@CountryName,"
                + "Phone=@Phone,"
                + "Recipient=@Recipient,"
                + "IsMainAddress=@IsMainAddress,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE EducaterNo=@myId "
                + "AND AddressNo=@myAddressNo";

        private string qryDelete = "DELETE EDUCATER_ADDRESS WHERE EducaterNo=@myId AND AddressNo=@myAddressNo";

    }
}
