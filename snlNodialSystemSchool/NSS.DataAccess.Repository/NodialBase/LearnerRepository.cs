﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class LearnerRepository : GenericRepository<Learner>, ILearnerRepository
    {
        public LearnerRepository(string connectionString)
            : base(connectionString){

        }

        public new Learner GetById(int id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Learner>().Where(learner => learner.LearnerNo.Equals(id)).First();
            }
        }

        public new bool Update(Learner learner){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        StatusCode = learner.StatusCode,

                        Gender = learner.Gender,
                        FirstName = learner.FirstName,
                        LastName = learner.LastName,
                        MiddleName = learner.MiddleName,
                        //FullName = learner.FullName,
                        LearnerEmail = learner.LearnerEmail,
                        LicenseNumber = learner.LicenseNumber,
                        Phone1 = learner.Phone1,
                        Phone2 = learner.Phone2,
                        Info1 = learner.Info1,
                        Info2 = learner.Info2,
                        Info3 = learner.Info3,
                        Notes = learner.Notes,
                        ExternalId = learner.ExternalId,
                        Blocked = learner.Blocked,
                        CreationDate = learner.CreationDate,
                        CreatedBy = learner.CreatedBy,
                        //ChangeDate = learner.ChangeDate,
                        //ModifiedBy = learner.ModifiedBy,

                        myId = learner.LearnerNo
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Remove(int id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE LEARNER SET "
                + "StatusCode=@StatusCode,"
                + "Gender=@Gender,"
                + "FirstName=@FirstName,"
                + "LastName=@LastName,"
                + "MiddleName=@MiddleName,"

                + "FullName=RTRIM(LTRIM(RTRIM(LTRIM(@FirstName + ' ' + @LastName)) + ' ' + ISNULL(@MiddleName,''))),"

                + "LearnerEmail=@LearnerEmail,"
                + "LicenseNumber=@LicenseNumber,"
                + "Phone1=@Phone1,"
                + "Phone2=@Phone2,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN' "

                + "WHERE LearnerNo=@myId";

        private string qryDelete = "DELETE LEARNER WHERE LearnerNo=@myId";

    }
}
