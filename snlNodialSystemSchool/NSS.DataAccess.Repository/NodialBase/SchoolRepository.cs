﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class SchoolRepository : GenericRepository<School>, ISchoolRepository
    {
        public SchoolRepository(string connectionString)
            : base(connectionString){

        }

        //public IEnumerable<School> GetAll(){
        //    using (var connection = new SqlConnection(_connectionString)){
        //        var result = connection.Query<School>(qrySelect).ToList();
        //        return result;
        //    }
        //}

        public School GetById(int id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<School>().Where(school => school.SchoolNo.Equals(id)).First();
            }
        }

        public new bool Update(School school){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        SchoolName = school.SchoolName,
                        SchoolType = school.SchoolType,
                        City = school.City,
                        State = school.State,
                        ZipCode = school.ZipCode,
                        Directions = school.Directions,
                        Address1 = school.Address1,
                        Address2 = school.Address2,
                        Address3 = school.Address3,
                        Phone1 = school.Phone1,
                        Phone2 = school.Phone2,
                        SchoolEmail = school.SchoolEmail,
                        Fax = school.Fax,
                        OpenDate = school.OpenDate,
                        CloseDate = school.CloseDate,
                        Info1 = school.Info1,
                        Info2 = school.Info2,
                        Info3 = school.Info3,
                        Notes = school.Notes,
                        ExternalId = school.ExternalId,
                        ActiveStatus = school.ActiveStatus,
                        CreationDate = school.CreationDate,
                        CreatedBy = school.CreatedBy,
                        //ChangeDate = school.ChangeDate,
                        //ModifiedBy = school.ModifiedBy,

                        myId = school.SchoolNo
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Remove(int id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private const string qrySelect = "SELECT * FROM SCHOOL";

        private const string qryUpdate = "UPDATE SCHOOL SET "
                + "SchoolName=@SchoolName,"
                + "SchoolType=@SchoolType,"
                + "City=@City,"
                + "State=@State,"
                + "ZipCode=@ZipCode,"
                + "Directions=@Directions,"
                + "Address1=@Address1,"
                + "Address2=@Address2,"
                + "Address3=@Address3,"
                + "Phone1=@Phone1,"
                + "Phone2=@Phone2,"
                + "SchoolEmail=@SchoolEmail,"
                + "Fax=@Fax,"
                + "OpenDate=@OpenDate,"
                + "CloseDate=@CloseDate,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "ActiveStatus=@ActiveStatus,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN' "

                + "WHERE SchoolNo=@myId";

        private const string qryDelete = "DELETE SCHOOL WHERE SchoolNo=@myId";

    }
}
