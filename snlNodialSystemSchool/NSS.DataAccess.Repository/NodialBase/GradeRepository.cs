﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class GradeRepository : GenericRepository<Grade>, IGradeRepository
    {
        public GradeRepository(string connectionString)
            : base(connectionString){

        }

        public Grade GetById(string id)
        {
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Grade>().Where(grade => grade.GradeCode.Equals(id)).First();
            }
        }

        public new bool Update(Grade grade){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        StatusCode = grade.SeasonCode,

                        GradeName = grade.GradeName,
                        Info1 = grade.Info1,
                        Info2 = grade.Info2,
                        Info3 = grade.Info3,
                        Notes = grade.Notes,
                        ExternalId = grade.ExternalId,
                        Blocked = grade.Blocked,
                        CreationDate = grade.CreationDate,
                        CreatedBy = grade.CreatedBy,
                        //ChangeDate = grade.ChangeDate,
                        //ModifiedBy = grade.ModifiedBy,

                        myId = grade.GradeCode
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE GRADE SET "
                + "SeasonCode=@SeasonCode,"    
                + "GradeName=@GradeName,"
                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE GradeCode=@myId";

        private string qryDelete = "DELETE GRADE WHERE GradeCode=@myId";
    }
}
