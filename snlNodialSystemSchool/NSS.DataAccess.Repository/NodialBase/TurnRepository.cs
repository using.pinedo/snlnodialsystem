﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class TurnRepository : GenericRepository<Turn>, ITurnRepository
    {
        public TurnRepository(string connectionString)
            : base(connectionString){

        }

        public Turn GetById(string id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Turn>().Where(turn => turn.TurnCode.Equals(id)).First();
            }
        }

        public new bool Update(Turn turn)
        {
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        SubGradeCode = turn.SubGradeCode,
                        GradeCode = turn.GradeCode,
                        SeasonCode = turn.SeasonCode,

                        SemesterName = turn.TurnName,
                        Info1 = turn.Info1,
                        Info2 = turn.Info2,
                        Info3 = turn.Info3,
                        Notes = turn.Notes,
                        ExternalId = turn.ExternalId,
                        Blocked = turn.Blocked,
                        CreationDate = turn.CreationDate,
                        CreatedBy = turn.CreatedBy,
                        //ChangeDate = turn.ChangeDate,
                        //ModifiedBy = turn.ModifiedBy,

                        myId = turn.TurnCode
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE TURN SET "
                + "SubGradeCode=@SubGradeCode,"
                + "GradeCode=@GradeCode,"
                + "SeasonCode=@SeasonCode,"
                + "TurnName=@TurnName,"

                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE TurnCode=@myId";

        private string qryDelete = "DELETE TURN WHERE TurnCode=@myId";
    }
}
