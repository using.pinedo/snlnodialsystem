﻿using NSS.Entity.Base;
using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq.Expressions;

namespace NSS.DataAccess.Repository.NodialBase
{
    public class SeasonRepository : GenericRepository<Season>, ISeasonRepository
    {
        public SeasonRepository(string connectionString)
            : base(connectionString){

        }

        public Season GetById(string id){
            using (var connection = new SqlConnection(_connectionString)){
                return connection.GetAll<Season>().Where(season => season.SeasonCode.Equals(id)).First();
            }
        }

        public new bool Update(Season season){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryUpdate,
                    new{
                        SeasonName = season.SeasonName,
                        StartDate = season.StartDate,
                        EndDate = season.EndDate,
                        StartTime = season.StartTime,
                        EndTime = season.EndTime,
                        Info1 = season.Info1,
                        Info2 = season.Info2,
                        Info3 = season.Info3,
                        Notes = season.Notes,
                        ExternalId = season.ExternalId,
                        Blocked = season.Blocked,
                        CreationDate = season.CreationDate,
                        CreatedBy = season.CreatedBy,
                        //ChangeDate = learner.ChangeDate,
                        //ModifiedBy = learner.ModifiedBy,

                        myId = season.SeasonCode
                    });
                return Convert.ToBoolean(result);
            }
        }

        public bool Delete(string id){
            using (var connection = new SqlConnection(_connectionString)){
                var result = connection.Execute(qryDelete, new { myId = id });
                return Convert.ToBoolean(result);
            }
        }

        private string qryUpdate = "UPDATE SEASON SET "
                + "SeasonName=@SeasonName,"
                + "StartDate=@StartDate,"
                + "EndDate=@EndDate,"
                + "StartTime=@StartTime,"
                + "EndTime=@EndTime,"

                + "Info1=@Info1,"
                + "Info2=@Info2,"
                + "Info3=@Info3,"
                + "Notes=@Notes,"
                + "ExternalId=@ExternalId,"
                + "Blocked=@Blocked,"

                + "CreationDate=@CreationDate,"
                + "CreatedBy=@CreatedBy,"

                + "ChangeDate=GETDATE(),"
                + "ModifiedBy='SYSADMIN',"

                + "WHERE SeasonCode=@myId";

        private string qryDelete = "DELETE SEASON WHERE SeasonCode=@myId";
    }
}
