﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NSS.Application.Web.Startup))]
namespace NSS.Application.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
