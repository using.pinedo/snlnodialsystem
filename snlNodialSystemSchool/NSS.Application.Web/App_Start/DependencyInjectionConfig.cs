﻿using log4net;
using log4net.Core;
using Microsoft.Owin;//
using Microsoft.Owin.Security;//
using NSS.DataAccess.Repository;
using NSS.Interface.Repository;
using SimpleInjector;
using SimpleInjector.Advanced;//
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace NSS.Application.Web.App_Start
{
    public class DependencyInjectionConfig
    {
        public static void ConfigureInjector(){
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<IUnitOfWork>(() => new UnitOfWork(ConfigurationManager.ConnectionStrings["NodialDataConnection"].ConnectionString));

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            //log4net
            container.RegisterConditional(typeof(ILog), c => typeof(Log4NetAdapter<>).MakeGenericType(c.Consumer.ImplementationType), Lifestyle.Singleton, c => true);

            //container.RegisterPerWebRequest<IAuthenticationManager>(() => AdvancedExtensions.IsVerifying(container) ? new OwinContext(new Dictionary<string, object>()).Authentication : HttpContext.Current.GetOwinContext().Authentication);
            //container.Register<IUserStore<AppUser, string>>(() => new UserStore<AppUser>>(),Lifestyle.Scoped);
            //container.Register<UserManager<AppUser, string>>(() => new UserManager<AppUser, string>(new UserStore<AppUser>()),Lifestyle.Scoped);

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }

    public sealed class Log4NetAdapter<T> : LogImpl{
        public Log4NetAdapter() : base(LogManager.GetLogger(typeof(T)).Logger) { }
    }
}