﻿using log4net;
using NSS.Application.Web.Controllers;
using NSS.Entity.Base;
using NSS.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSS.Application.Web.Areas.SchoolModule.Controllers
{
    public class SchoolController : BaseController
    {
        public SchoolController(IUnitOfWork unit, ILog log)
            : base(unit, log){

        }
        public ActionResult Index(){
            _log.Info("Start Index SchoolController");
            return View(_unit.School.GetAll());
        }

        public PartialViewResult Details(int id){
            _log.Info("Start Details SchoolController");
            return PartialView("Details", _unit.School.GetById(id));
        }

        public PartialViewResult Create(){
            _log.Info("Start Create SchoolController");
            return PartialView("Create", new School());
        }

        [HttpPost]
        public ActionResult Create(School school){
            _log.Info("Start Post Create SchoolController");
            if (ModelState.IsValid){
                _unit.School.add(school);
                return RedirectToAction("Index");
            }

            return PartialView(school);
        }

        public PartialViewResult Update(int id){
            _log.Info("Start Update SchoolController");
            return PartialView("Update", _unit.School.GetById(id));
        }

        [HttpPost]
        public ActionResult Update(School school){
            _log.Info("Start Post Update SchoolController");
            if (_unit.School.Update(school)) return RedirectToAction("Index");

            return PartialView(school);
        }

        public PartialViewResult Deleted(int id){
            _log.Info("Start Deleted SchoolController");
            return PartialView("Deleted", _unit.School.GetById(id));
        }

        [HttpPost]
        [ActionName("Deleted")]
        public ActionResult DeletedPost(int id){
            _log.Info("Start Post Deleted SchoolController");
            if (_unit.School.Remove(id)) return RedirectToAction("Index");

            return PartialView("Deleted", _unit.School.GetById(id));
        }

	}
}