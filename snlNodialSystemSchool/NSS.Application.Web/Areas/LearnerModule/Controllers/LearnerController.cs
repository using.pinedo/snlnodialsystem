﻿using log4net;
using NSS.Application.Web.Controllers;
using NSS.Entity.Base;
using NSS.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSS.Application.Web.Areas.LearnerModule.Controllers
{
    public class LearnerController : BaseController
    {
        public LearnerController(IUnitOfWork unit, ILog log)
            : base(unit, log){

        }
        public ActionResult Index(){
            _log.Info("Start Index LearnerController");
            return View(_unit.Learner.GetAll());
        }

        public PartialViewResult Details(int id){
            _log.Info("Start Details LearnerController");
            return PartialView("Details", _unit.Learner.GetById(id));
        }

        public PartialViewResult Create(){
            _log.Info("Start Create LearnerController");
            return PartialView("Create", new Learner());
        }

        [HttpPost]
        public ActionResult Create(Learner learner){
            _log.Info("Start Post Create LearnerController");
            if (ModelState.IsValid){
                _unit.Learner.add(learner);
                return RedirectToAction("Index");
            }

            return PartialView(learner);
        }

        public PartialViewResult Update(int id){
            _log.Info("Start Update LearnerController");
            return PartialView("Update", _unit.Learner.GetById(id));
        }

        [HttpPost]
        public ActionResult Update(Learner learner){
            _log.Info("Start Post Update LearnerController");
            if (_unit.Learner.Update(learner)) return RedirectToAction("Index");

            return PartialView(learner);
        }

        public PartialViewResult Deleted(int id){
            _log.Info("Start Deleted LearnerController");
            return PartialView("Deleted", _unit.Learner.GetById(id));
        }

        [HttpPost]
        [ActionName("Deleted")]
        public ActionResult DeletedPost(int id){
            _log.Info("Start Post Deleted LearnerController");
            if (_unit.Learner.Remove(id)) return RedirectToAction("Index");

            return PartialView("Deleted", _unit.Learner.GetById(id));
        }

	}
}