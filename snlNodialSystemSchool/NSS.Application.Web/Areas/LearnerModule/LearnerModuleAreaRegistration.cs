﻿using System.Web.Mvc;

namespace NSS.Application.Web.Areas.LearnerModule
{
    public class LearnerModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "LearnerModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "LearnerModule_default",
                "LearnerModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}