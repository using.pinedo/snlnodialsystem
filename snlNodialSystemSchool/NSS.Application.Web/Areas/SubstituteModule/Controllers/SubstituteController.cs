﻿using log4net;
using NSS.Application.Web.Controllers;
using NSS.Entity.Base;
using NSS.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSS.Application.Web.Areas.SubstituteModule.Controllers
{
    public class SubstituteController : BaseController
    {
        public SubstituteController(IUnitOfWork unit, ILog log)
            : base(unit, log){

        }
        public ActionResult Index(){
            _log.Info("Start Index SubstituteController");
            return View(_unit.Substitute.GetAll());
        }

        public PartialViewResult Details(int id){
            _log.Info("Start Details SubstituteController");
            return PartialView("Details", _unit.Substitute.GetById(id));
        }

        public PartialViewResult Create(){
            _log.Info("Start Create SubstituteController");
            return PartialView("Create", new Substitute());
        }

        [HttpPost]
        public ActionResult Create(Substitute substitute){
            _log.Info("Start Post Create SubstituteController");
            if (ModelState.IsValid){
                _unit.Substitute.add(substitute);
                return RedirectToAction("Index");
            }

            return PartialView(substitute);
        }

        public PartialViewResult Update(int id){
            _log.Info("Start Update SubstituteController");
            return PartialView("Update", _unit.Substitute.GetById(id));
        }

        [HttpPost]
        public ActionResult Update(Substitute substitute){
            _log.Info("Start Post Update SubstituteController");
            if (_unit.Substitute.Update(substitute)) return RedirectToAction("Index");

            return PartialView(substitute);
        }

        public PartialViewResult Deleted(int id){
            _log.Info("Start Deleted SubstituteController");
            return PartialView("Deleted", _unit.Substitute.GetById(id));
        }

        [HttpPost]
        [ActionName("Deleted")]
        public ActionResult DeletedPost(int id){
            _log.Info("Start Post Deleted SubstituteController");
            if (_unit.Substitute.Remove(id)) return RedirectToAction("Index");

            return PartialView("Deleted", _unit.Substitute.GetById(id));
        }

	}
}