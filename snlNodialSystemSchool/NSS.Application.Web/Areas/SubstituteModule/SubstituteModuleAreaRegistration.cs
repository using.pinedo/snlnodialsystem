﻿using System.Web.Mvc;

namespace NSS.Application.Web.Areas.SubstituteModule
{
    public class SubstituteModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SubstituteModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SubstituteModule_default",
                "SubstituteModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}