﻿using System.Web.Mvc;

namespace NSS.Application.Web.Areas.EducatorModule
{
    public class EducatorModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EducatorModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "EducatorModule_default",
                "EducatorModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}