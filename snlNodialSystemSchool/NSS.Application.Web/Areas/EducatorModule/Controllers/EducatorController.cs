﻿using log4net;
using NSS.Application.Web.Controllers;
using NSS.Entity.Base;
using NSS.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSS.Application.Web.Areas.EducatorModule.Controllers
{
    public class EducatorController : BaseController
    {
        public EducatorController(IUnitOfWork unit, ILog log)
            : base(unit, log){

        }
        public ActionResult Index(){
            _log.Info("Start Index EducatorController");
            return View(_unit.Educator.GetAll());
        }

        public PartialViewResult Details(int id){
            _log.Info("Start Details EducatorController");
            return PartialView("Details", _unit.Educator.GetById(id));
        }

        public PartialViewResult Create(){
            _log.Info("Start Create EducatorController");
            return PartialView("Create", new Educator());
        }

        [HttpPost]
        public ActionResult Create(Educator educator){
            _log.Info("Start Post Create EducatorController");
            if (ModelState.IsValid){
                _unit.Educator.add(educator);
                return RedirectToAction("Index");
            }

            return PartialView(educator);
        }

        public PartialViewResult Update(int id){
            _log.Info("Start Update EducatorController");
            return PartialView("Update", _unit.Educator.GetById(id));
        }

        [HttpPost]
        public ActionResult Update(Educator educator){
            _log.Info("Start Post Update EducatorController");
            if (_unit.Educator.Update(educator)) return RedirectToAction("Index");

            return PartialView(educator);
        }

        public PartialViewResult Deleted(int id){
            _log.Info("Start Deleted EducatorController");
            return PartialView("Deleted", _unit.Educator.GetById(id));
        }

        [HttpPost]
        [ActionName("Deleted")]
        public ActionResult DeletedPost(int id){
            _log.Info("Start Post Deleted EducatorController");
            if (_unit.Educator.Remove(id)) return RedirectToAction("Index");

            return PartialView("Deleted", _unit.Educator.GetById(id));
        }

	}
}