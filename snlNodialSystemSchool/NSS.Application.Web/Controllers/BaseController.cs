﻿using log4net;
using NSS.Application.Web.Action_Filter;
using NSS.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSS.Application.Web.Controllers
{
    [ErrorActionFilter]
    public class BaseController : Controller
    {
        protected readonly IUnitOfWork _unit;
        protected readonly ILog _log;
        public BaseController(IUnitOfWork unit, ILog log){
            _unit = unit;
            _log = log;
        }

	}
}