﻿(function (NSSApplication) {
    NSSApplication.getModal = getModalContent;
    NSSApplication.closeModal = closeModal;
    return NSSApplication;

    function getModalContent(url) {
        $.get(url, function (data) {
            $('.modal-body').html(data);
        });
    }
    
    function closeModal(option) {        
        $("button[data-dismiss='modal']").click();        
        $('.modal-body').html("");        

        modifyAlertsClassess(option);
        
        function modifyAlertsClassess(option) {
            $('#createMessage').addClass('hidden');
            $('#editMessage').addClass('hidden');
            $('#deleteMessage').addClass('hidden');            

            if (option === 'create') {
                $('#createMessage').removeClass('hidden');
            }
            else if (option === 'update') {
                $('#editMessage').removeClass('hidden');
            }
            else if (option === 'delete') {
                $('#deleteMessage').removeClass('hidden');

            }

            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(1000, function () {
                    $(this).remove();
                });
            }, 5000);
        }
    }

})(window.NSSApplication = window.NSSApplication || {});