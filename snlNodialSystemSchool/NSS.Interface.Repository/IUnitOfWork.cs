﻿using NSS.Interface.Repository.INodialBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Interface.Repository
{
    public interface IUnitOfWork// : IDisposable
    {
        IClassRoomRepository ClassRoom { get; }
        IEducatorRepository Educator { get; }
        IEducaterAddressRepository EducaterAddress { get; }
        IEducaterExploitRepository EducaterExploit { get; }
        IEducaterRecordsRepository EducaterRecords { get; }
        IEmployeeRepository Employee { get; }
        IGradeRepository Grade { get; }
        ILearnerRepository Learner { get; }
        ILearnerRecordsRepository LearnerRecords { get; }
        ISchoolRepository School { get; }
        ISeasonRepository Season { get; }
        ISemesterRepository Semester { get; }
        ISubGradeRepository SubGrade { get; }
        ISubjectRepository Subject { get; }
        ISubstituteRepository Substitute { get; }
        ISubstituteAddressRepository SubstituteAddress { get; }
        ITurnRepository Turn { get; }
        //int Complete();
    }
}
