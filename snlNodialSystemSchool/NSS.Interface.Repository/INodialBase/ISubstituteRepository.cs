﻿using NSS.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Interface.Repository.INodialBase
{
    public interface ISubstituteRepository : IGenericRepository<Substitute>
    {
        new Substitute GetById(int id);
        new bool Update(Substitute substitute);
        bool Remove(int id);
    }
}
