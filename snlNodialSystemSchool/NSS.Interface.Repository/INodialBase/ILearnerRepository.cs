﻿using NSS.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Interface.Repository.INodialBase
{
    public interface ILearnerRepository : IGenericRepository<Learner>
    {
        //IEnumerable<Learner> GetAll();
        new Learner GetById(int id);
        new bool Update(Learner learner);
        bool Remove(int id);
    }
}
