﻿using NSS.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Interface.Repository.INodialBase
{
    public interface ISchoolRepository : IGenericRepository<School>
    {
        IEnumerable<School> GetAll();
        School GetById(int id);
        new bool Update(School school);
        bool Remove(int id);
    }
}
