﻿using NSS.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Interface.Repository.INodialBase
{
    public interface IEducatorRepository : IGenericRepository<Educator>
    {
        new Educator GetById(int id);
        new bool Update(Educator educator);
        bool Remove(int id);
    }
}
