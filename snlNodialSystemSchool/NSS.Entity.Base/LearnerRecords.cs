﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Entity.Base
{
    [Table("LEARNER_RECORDS")]
    public partial class LearnerRecords
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LearnerNo { get; set; }
    }
}
