﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Entity.Base
{
    [Table("TURN")]
    public partial class Turn
    {
        [Key]
        [StringLength(5)]
        public string TurnCode { get; set; }

        [Key]
        [StringLength(5)]
        public string SubGradeCode { get; set; }

        [Key]
        [StringLength(5)]
        public string GradeCode { get; set; }

        [Key]
        [StringLength(5)]
        public string SeasonCode { get; set; }

        [StringLength(30)]
        public string TurnName { get; set; }

        [StringLength(100)]
        public string Info1 { get; set; }

        [StringLength(100)]
        public string Info2 { get; set; }

        [StringLength(100)]
        public string Info3 { get; set; }

        [StringLength(500)]
        public string Notes { get; set; }

        [StringLength(50)]
        public string ExternalId { get; set; }

        public bool Blocked { get; set; }

        public DateTime? CreationDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? ChangeDate { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }
    }
}
