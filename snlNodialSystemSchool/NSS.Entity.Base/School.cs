﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Extensions;
//using Dapper.Contrib.Extensions;

namespace NSS.Entity.Base
{
    [Table("SCHOOL")]
    public partial class School
    {
        public int id { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SchoolNo { get; set; }

        [StringLength(50)]
        public string SchoolName { get; set; }

        [StringLength(1)]
        public string SchoolType { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }

        [StringLength(1000)]
        public string Directions { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(200)]
        public string Address3 { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(100)]
        public string SchoolEmail { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        public DateTime? OpenDate { get; set; }

        public DateTime? CloseDate { get; set; }

        [StringLength(100)]
        public string Info1 { get; set; }

        [StringLength(100)]
        public string Info2 { get; set; }

        [StringLength(100)]
        public string Info3 { get; set; }

        [StringLength(2000)]
        public string Notes { get; set; }

        [StringLength(50)]
        public string ExternalId { get; set; }

        public bool ActiveStatus { get; set; }

        public DateTime? CreationDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? ChangeDate { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }
    }
}
