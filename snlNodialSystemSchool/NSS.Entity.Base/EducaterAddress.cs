﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Entity.Base
{
    [Table("EDUCATER_ADDRESS")]
    public partial class EducaterAddress
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EducaterNo { get; set; }

        [Key]
        [Column(Order = 1)]
        public int AddressNo { get; set; }

        [StringLength(1)]
        public string StatusCode { get; set; }

        [StringLength(30)]
        public string Label { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [StringLength(10)]
        public string State { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }

        [StringLength(100)]
        public string CountryName { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Recipient { get; set; }

        [StringLength(100)]
        public string Info1 { get; set; }

        [StringLength(100)]
        public string Info2 { get; set; }

        [StringLength(100)]
        public string Info3 { get; set; }

        [StringLength(2000)]
        public string Notes { get; set; }

        public bool IsMainAddress { get; set; }

        public DateTime? CreationDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? ChangeDate { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }
    }
}
