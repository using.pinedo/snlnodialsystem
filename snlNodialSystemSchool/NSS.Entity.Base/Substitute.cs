﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSS.Entity.Base
{
    [Table("SUBSTITUTE")]
    public partial class Substitute
    {
        public int Id { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SubstituteNo { get; set; }

        [StringLength(1)]
        public string StatusCode { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(10)]
        public string Title { get; set; }

        [StringLength(40)]
        public string FirstName { get; set; }

        [StringLength(40)]
        public string LastName { get; set; }

        [StringLength(40)]
        public string MiddleName { get; set; }

        [StringLength(40)]
        public string MaidenName { get; set; }

        [StringLength(30)]
        public string LicenseNumber { get; set; }

        [StringLength(100)]
        public string SubstituteEmail { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(100)]
        public string Info1 { get; set; }

        [StringLength(100)]
        public string Info2 { get; set; }

        [StringLength(100)]
        public string Info3 { get; set; }

        [StringLength(2000)]
        public string Notes { get; set; }

        [StringLength(50)]
        public string ExternalId { get; set; }

        public bool Blocked { get; set; }

        public DateTime? CreationDate { get; set; }

        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime? ChangeDate { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }
    }
}
